import Vue from "vue";

Vue.mixin({
  methods:{
    async callApi(method,url, data){
      try {
        const res =  await this.$axios({
          method,
          url,
          data
        })
        return res.data
      } catch (error) {
        console.log(error, "eror");
      }
    }
  }
})