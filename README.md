# S A M A R A

Site deployed on: 
[Website](https://samarasamara.netlify.app/)


## Description

Single Page Application created using NuxtJS.

## Features & tech used

* NuxtJS
* SASS (CSS Preprocessors)
* Axios (HTTP Request)
* GreenSock Animation Platform ( Animation )
* Vue Mixins
* Instagram API
* Responsive
* Netlify

## Installations

```bash
# clone this repository
$ git clone git@gitlab.com:raihanrachman31/samara-test.git

# move to the project
$ cd samara-test

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Sources

* [NuxtJS](https://nuxtjs.org/)
* [Nuxt - Axios](https://id.nuxtjs.org/guide/plugins/)
* [Nuxt - SASS](https://id.nuxtjs.org/faq/pre-processors/)
* [GSAP](https://greensock.com/get-started/#tweening-basics)
* [GSAP - Vue](https://blog.usejournal.com/vue-js-gsap-animations-26fc6b1c3c5a)
* [Instagram API](https://developers.facebook.com/docs/instagram-api/getting-started)






